/**
 * Created by timmeno1 on 28.12.2017.
 */
// import gulp from 'gulp';

const gulp = require("gulp");
const sourcemaps = require("gulp-sourcemaps");
const babel = require("gulp-babel");
const concat = require("gulp-concat");
const { fork } = require('child_process');
const del = require('del');

const paths = {
    destDir : 'dist',
    srcjs : 'src/**/*.js',
    srcrest : ['src/**/*', '!src/routes/*.js', '!src/schema/*.js', '!src/bin/*.js', '!src/*.js'],
    srcserve : 'src/**/*'
};

gulp.task('clean', () => {
    return del(paths.destDir);
});

gulp.task("copy", ["clean"], function() {
    return gulp.src(paths.srcrest)
        .pipe(gulp.dest(paths.destDir));
});

gulp.task("build", ["copy"], function () {
    return gulp.src(paths.srcjs)
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(paths.destDir));
});

gulp.task("run", ()=>{
    return child = fork('dist/bin/www', [], {stdio : 'inherit'});
});
