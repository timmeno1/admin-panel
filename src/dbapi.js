/**
 * Created by timmeno1 on 06.01.2018.
 */
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/myapp');
let User = require('./schema/user').User;
let Message = require('./schema/message').Message;



// User API
exports.createUser = function(userData){

        let user = new User({
            email : userData.email,
            password : userData.passwd,
            firstName : userData.fname,
            secondName : userData.sname
        });
        user.save();

};

exports.getUser = function(email){
        return User.findOne({email: email })
};


//Chat API

exports.createMsg = function(sender, receiver, message){
    let msg = {
        sender : sender,
        receiver : receiver ,
        message : message
    };
    return new Message(msg).save();
};
