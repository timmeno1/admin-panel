/**
 * Created by timmeno1 on 06.01.2018.
 */
const mongoose = require('mongoose');

let messageSchema = new mongoose.Schema({
    sender : {
        type : Number,
        required : true
    },
    receiver : {
        type : Number,
        required : true
    },
    date : {
        type : Date,
        default : Date.now
    },
    message : String
});


module.exports = mongoose.model('Message', messageSchema);