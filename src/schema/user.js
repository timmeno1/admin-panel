/**
 * Created by timmeno1 on 06.01.2018.
 */
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/myapp');

let userSchema = new mongoose.Schema({
    email : {
        type : String,
        unique : true,
        required : true
    },
    password : {
        type : String,
        required : true
    },
    firstName : String,
    secondName : String,
    country : String,
    birthDate : Number
});


exports.User = mongoose.model('User', userSchema );
