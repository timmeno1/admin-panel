/**
 * Created by timmeno1 on 05.01.2018.
 */
var express = require('express');
var router = express.Router();

router.get('/', (req,res,next)=>{
    res.render('login', {title:'Login page'});
});

router.post('/', (req,res,next)=>{
    console.log(req.body);
});

module.exports = router;