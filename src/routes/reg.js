/**
 * Created by timmeno1 on 05.01.2018.
 */
var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
let db = mongoose.connect('mongodb://localhost/myapp');
var dbapi = require('../dbapi');

router.get('/', (req,res,next)=>{
    res.render('reg', {title: 'Registration Page'});
});

router.post('/', (req,res,next)=> {
    try {
        dbapi.createUser(req.body);
    } catch (err) {
        console.log(err)
    }

});


module.exports = router;